if (top.frames.length) {
	var navArr = [];

	$('#nav', parent.document)
		.find('li').removeClass('is-active')
		.find('a').each(function() {
			var self = $(this),
				father = self.parents('.nav-item');

			if (location.href.indexOf(self.attr('href')) > 0) {
				father.addClass('is-active');

				if (self.parent().is('.nav-item')) {
					navArr.push(self.text());
				} else {
					self.parent().addClass('is-active');
					navArr.push(father.children('a').text());
					navArr.push(self.text());
				}

				buildBreadcrumb(navArr);
				navArr.length = 0;

				return false;
			}
		});
}

/**
 * 构建面包屑
 * @param {array} navArr - 栏目数组
 */
function buildBreadcrumb(navArr) {
	var title = '',
		html = '';
	for (var i = 0; i < navArr.length; i++) {
		var lastIdx = navArr.length - 1 - i;
		title += navArr[lastIdx] + (lastIdx === 0 ? '' : ' | ');
		html += '<span class="' +
					(i === navArr.length - 1 ? 'is-active' : '') + '">' +
					navArr[i] +
				'</span>';
	}

	var oldTitle = parent.document.title.split(' - ')[1] || parent.document.title;
	parent.document.title = title + ' - ' + oldTitle;  // 浏览器标题
	$('.crumb:eq(0)').html(html);
}

(function($) {
	// 消息提示
	$.fn.msg = function(options) {
		var defaults = {
			type: 'normal',
			text: '',
			icon: '',
			duration: 260,
			delay: 3000
		};

		var opts = $.extend({}, defaults, options);
		return this.each(function() {
			var self = $(this),
				destroy = function(obj) {
					obj.fadeOut(opts.duration, function() {
						obj.remove();
					});
				};

			if (options === 'close') {
				self.empty();
				return;
			}

			var msgTypeIcons = {
				normal: 'icon-info',
				success: 'icon-success_fill',
				warning: 'icon-prompt_fill',
				error: 'icon-warning_fill'
			};

			var buildMsg = function(wrap) {
				var type = opts.type,
					icon = opts.icon;

				if (!msgTypeIcons.hasOwnProperty(type)) {  // type 定义非法
					type = defaults.type;
				}

				if (!icon) {
					icon = msgTypeIcons[type];
				}

				var html = '<div class="msg msg-' + type + '" style="display:none">'
					+		'<i class="iconfont ' + icon + '"></i>'
					+ 		'<p>' + opts.text + '</p>'
					+	'</div>';

				wrap.append(html);

				delayMsg(wrap);
			};

			var delayMsg = function(wrap) {
				var msg = wrap.children('.msg').last();

				msg.fadeIn(opts.duration, function() {
					if (opts.delay) {
						setTimeout(function() {
							destroy(msg);
						}, opts.delay);
					}
				});
			};

			buildMsg(self);
		});
	}

	// 选项卡
	$.fn.tabs = function() {
		return this.each(function() {
			var self = $(this),
				header = self.children('.tabs-title'),
				content = self.children('.tabs-cont');

			header.on('click', 'a', function(e) {
				e.preventDefault();

				var self = $(this),
					selector = self.attr('href');

				if (self.hasClass('active')) {
					return;
				}

				self.parent('li').addClass('active').siblings().removeClass('active');
				$(selector).addClass('active').siblings().removeClass('active');
			});
		});
	}
	$('.tabs').tabs();  // 调用
})(jQuery);
